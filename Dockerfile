FROM rust:alpine

RUN apk update && \
    apk add cargo-audit musl-dev lua5.4 lua5.4-dev git

RUN rustup component add clippy
RUN rustc --version && cargo --version