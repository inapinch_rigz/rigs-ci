# rigs-ci

Base Docker Image for [rigz](https://gitlab.com/inapinch_rigz/rigz) CI Pipeline, includes the core utils to build and test.

## Dependencies
- cargo-audit
- git2 - perl make pkgconfig 
- base linking - musl-dev libc-dev gcc
- Zig - zig@edgecommunity